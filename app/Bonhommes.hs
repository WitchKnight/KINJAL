{-# LANGUAGE TemplateHaskell,DoAndIfThenElse, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}

module Bonhommes where

import GHC.Generics

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import Data.HashMap.Strict((!))
import qualified Data.HashMap.Strict as DHS
import Data.Hashable
import Linear.V2
import Data.IORef

import Control.Lens
import Control.Monad

import Lib
import Animations
import Conf
import View


data BnState = Idle
             | Attack Skill
             | Walking
             deriving (Show,Read,Eq,Ord,Generic)

data TotalState = TS BnState Time 

instance Hashable BnState


data Skill = Skill {
  _duration :: Float, -- in seconds
  _hitPoint :: [(Int, Coord,Maybe Coord)] -- number of frames, how much the player moves, where the point is if any . 
} deriving (Show,Read,Eq,Ord,Generic)

instance Hashable Skill
makeLenses ''Skill

data Bonhomme = Bonhomme {
  _bonPos   :: IORef Coord,
  _bonSkills:: [Skill],
  _bonAnim  :: DHS.HashMap BnState Animation,
  _bonState :: IORef TotalState,
  _bonDir   :: IORef Int
}

makeLenses ''Bonhomme

createBonh listate pos = do
  let anms = DHS.fromList listate
  let liskills = [skill | (Attack skill, _) <- listate]
  state <- newIORef $ TS Idle  timeZero
  crd <- newIORef pos
  dir <- newIORef 5
  return $ Bonhomme {
    _bonPos = crd,
    _bonAnim = anms,
    _bonState = state,
    _bonDir = dir,
    _bonSkills = liskills
    } 

getPos bonh = do
  pos <- readIORef $ bonh^.bonPos
  return pos

getState bonh = do
  state <- readIORef (bonh^.bonState) 
  return state

getAnim bonh = do
  TS state time <- getState bonh
  return $ (bonh^.bonAnim) ! state

getStateAnim state bonh = do
  return  $ (bonh^.bonAnim) ! state

setState bonh state = do
  writeIORef (bonh^.bonState) (TS state 0)

tryState bonh state = do
  TS currentState time <- getState bonh
  a <- case currentState of
    Attack _   -> return False
    _ -> do
      setState bonh state
      return True
  return a

setTotalState bonh state = do
  writeIORef (bonh^.bonState) state

updateBonAnim dt bonh = do
  anim <- getAnim bonh
  updateAnim dt anim

updateTs dt (TS st time) = TS st (time+dt)

getDir bonh = readIORef $ bonh^.bonDir

moveB dt bonh = do
  dir <- readIORef $ bonh^.bonDir
  when (dir == 6) (moveE dt bonh)
  when (dir == 4) (moveW dt bonh)
  return ()

moveE dt bonh = do
  modifyIORef (bonh^.bonPos) (+ (V2 (asSeconds dt) 0 ))


moveW dt bonh = do
  modifyIORef (bonh^.bonPos) (+ (V2 ( - (asSeconds dt)) 0 ))
  
beginMove dir bonh = writeIORef (bonh^.bonDir) dir


updateState dt bonh = do
  state <- getState bonh
  newState <- case state of
    TS (Attack skl) time -> if asSeconds time > (skl^.duration) then
                        return $ TS Idle 0
                      else
                        return $ updateTs dt state
    TS Walking time -> if asSeconds time < 0.66 then 
                          do
                            moveB dt bonh
                            return $ updateTs dt state
                        else
                          return $ TS Idle 0
    TS Idle _       -> do
                        dir <- getDir bonh
                        if (dir /=5) then
                          return $ TS Walking 0
                        else
                          return $ updateTs dt state
    other       -> return other
  setTotalState bonh newState

updateBonh dt bonh= do
  updateBonAnim dt bonh
  updateState dt bonh
  
cleanBonhomme bonh = do
  getStateAnim Idle bonh >>= cleanAnim 
  getStateAnim Walking bonh >>= cleanAnim 
  forM (bonh^.bonSkills) $ \skill  -> getStateAnim (Attack skill) bonh >>= cleanAnim

instance Viewable Bonhomme where
  drawWithTransform tg transf bonh = do
    V2 x y <- getPos bonh  
    let transf2 = translation (x*tSZ') (y*tSZ')
    anim <- getAnim bonh
    drawWithTransform tg (transf*transf2) anim


attack bonh skill = do
  ok <- tryState bonh (Attack skill)
  when ok $ do 
    anim <- getStateAnim (Attack skill) bonh
    resetAnim anim
  return ()

getSkAnim tex skill = let hp = (skill^.hitPoint)
                          durations = map (\x -> x^._1) hp
                          mov = map (\x -> x^._2) hp in
  initAnim tex durations mov
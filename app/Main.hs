{-# LANGUAGE OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module Main where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock

import Control.Monad
import Control.Concurrent

import System.IO.Unsafe
import Lib
import Game
import Animations
import Conf
import View
import GameData
import Game

import Actors
import Input
import Init
import App
main :: IO ()
main = do
  let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
  wnd   <- createRenderWindow (VideoMode gWIDTH gHEIGHT 32) "KINJAL" [SFDefaultStyle] ctxSettings
  clock <- createClock
  gS    <- initGame wnd
  loop clock timeZero gS
  cleanGame gS
  return ()



loop :: Clock -> Time -> GameS a ->  IO ()
loop clock dt gS@(InMenu c wnd env scene) = case c of
  Nothing -> do
    dt    <- restartClock clock
    gS <- updateGame dt gS
    drawGame gS
    evt   <- pollEvent wnd
    case evt of
      Just SFEvtClosed -> return ()
      Just (SFEvtMouseMoved x y) -> do
        s <- updateCursor x y scene
        loop clock dt (withSc s gS)
      Just (SFEvtMouseButtonPressed p x y) -> do
        gS <- confirmG gS
        loop clock dt gS
      _                -> loop clock dt gS
  Just x -> do
          when (x==1) $ do
            bS <- (beginBattle gS)
            loop clock dt bS
          when (x==0) $ do
            print "quitting game..."


loop clock dt gS@(InBattle c wnd gEnv scene) = case c of
  Nothing ->  do
    dt    <- restartClock clock
    time <- getTime scene
    gS <- updateGame dt gS
    clearRenderWindow wnd black
    drawG wnd gS
    display wnd
    evt   <- pollEvent wnd
    case evt of
      Just SFEvtClosed -> return ()
      Just (SFEvtKeyPressed code _ _ _ _) -> handlePress code gS  >>= loop clock dt 
      Just (SFEvtKeyReleased code _ _ _ _) -> handleRelease code gS  >>= loop clock dt 
      _                -> loop clock dt gS
  Just x -> do
    when (x==1) $ do
      print "switching scene"
      gS <- gameOver gS
      loop clock dt gS


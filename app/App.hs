{-# LANGUAGE ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module App where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time


import Debug.Trace
import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.List(find,elemIndex)
import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game
import Actors


data MenuS = MenuS {
  _sMenu    :: GMenu, 
  _msTime   :: IORef Time,
  _msCursor  :: Maybe (V2 Int)
}

type GMenu = Menu (GameS MenuS) MenuS
type GUI   = UIElem (GameS MenuS) MenuS

data GameS a where
  InMenu ::   Maybe Int -> RenderWindow -> GameEnv -> MenuS -> GameS MenuS
  InBattle :: Maybe Int -> RenderWindow -> GameEnv -> BattleS -> GameS BattleS


instance Scene MenuS where
  getTimeRef (MenuS _ t _) =  t
  drawWithEnv tg env s@(MenuS menu t _) = do
    HIO.mapM_ f (menu^.mElems)
    where
      f (crd,el) =  do
        time <- getTime s
        let V2 x y = getUIPos crd 
        let transf =  (translation x y)
        (el^.uiLabel) >?> \x -> do
          t <- getText (x^.lId) (env^.gameText)
          t >?> drawWithTransform tg transf
        dwtat tg time transf el

data BattleTile = Ground | Air deriving (Show,Read,Eq,Ord,Generic)
instance Hashable BattleTile

type Actors = HIO.LinearHashTable Int Actor

data BattleS = BattleS {
  _bsTime     :: IORef Time,
  _bsCursor   :: Maybe (V2 Int),
  _protag     :: Actor,
  _enemies    :: Actors,
  _tiles      :: HIO.LinearHashTable Coord BattleTile,
  _tileData   :: HIO.LinearHashTable BattleTile Int
}

makeLenses '' BattleS

instance Scene BattleS where
  getTimeRef bS =  bS^.bsTime
  updateScene dt bS = do
    let p = bS^.protag
    addTime dt bS
    updateActor dt p
    
    let checkSkl p act = do
                    (TS st t d) <- getState p
                    a <- case st of
                      InSkill i -> do
                        skl <- HIO.lookup (p^.aSkill) i
                        skl >?> \s -> do
                          let sData = getSklSkillData s t
                          let pmPos = displace sData
                          let hBoxes =  hitBoxes sData
                          -- print hBoxes
                          forM_ hBoxes $ \(HitBox pos size1) -> do
                            apmPos <- case pmPos of
                              Nothing -> return c0
                              Just (Displace x) -> return x
                            let finalOffset = negOrPos (d==4) $ pos + apmPos
                            ppos <- getPos p
                            let ap = (fmap (*tSZ') ppos)  + finalOffset
                            epp <- getRealPos act
                            when (touches ap epp (act^.actSize)) $ do
                              knockback act 0.2 d
                              TS st2 t2 d2 <- getState act
                              let ouch = print "ouch" >> softState act Hurt >>  modLife act (+(-(s^.damage* asMilliseconds dt)))
                              let checkSkill x = do
                                    print "checking skill"
                                    askill <- HIO.lookup (act^.aSkill) x
                                    askill >?> \s -> do
                                      let asData = getSklSkillData s t2
                                      let inv = invincible asData
                                      let par = parry asData
                                      
                                      a <- case (inv,par) of
                                        (_,Just (Parry n)) -> setState act (InSkill 4)
                                        (True,_)    -> return ()
                                        _           -> ouch
                                      return ()
                              a <- case st2 of 
                                InSkill x -> do
                                  checkSkill x
                                _ -> do
                                  ouch
                              return a
                      _ -> return ()
                    return a  
    let checkDth (i,act) = do
                            lf <- getLife act
                            when (lf <= 0) $ do
                              HIO.delete (bS^.enemies) i
    
      
    HIO.mapM_ (updateActor dt.snd) $ bS^.enemies 
    HIO.mapM_ ((\o ->(o^.aI.onSight)p o).snd)  $ bS^.enemies 
    HIO.mapM_ (checkSkl p.snd) $ bS^.enemies 
    HIO.mapM_ ((\a -> checkSkl a p).snd) $ bS^.enemies 
    HIO.mapM_ checkDth $ bS^.enemies 
    ens <- HIO.toList $ bS^.enemies 
    let allA = p : map snd ens
    return bS
  drawWithEnv tg gEnv bS = do
    time <- getTime bS 
    displayAct tg gEnv $ bS^.protag
    HIO.mapM_ (f time) (bS^.tiles) >> HIO.mapM_ (displayAct tg gEnv. snd) (bS^.enemies)
      where
        f time (crd,tile) = do
          let V2 x y = crd
          let transf = translation (x*tSZ') (y*tSZ')
          i <- HIO.lookup (bS^.tileData) tile
          i >?> \i -> do
            (anim :: Maybe Animation) <- getAnim gEnv i
            anim >?> dwtat tg time transf

makeLenses ''MenuS

instance GameState (GameS MenuS) MenuS where
  getOCode (InMenu c _ _ _) = c
  withOCode (InMenu _ w env scene) c = (InMenu (Just c) w env scene)
  envG (InMenu _ _ env _) = env
  scenG (InMenu _ _ _ s)  = s
  wndG (InMenu _ w env scene) = w
  withSc s (InMenu c w env scene) = (InMenu c w env s)

instance GameState (GameS BattleS) BattleS where
  getOCode (InBattle c _ _ _) = c
  withOCode (InBattle _ w env scene) c = (InBattle (Just c) w env scene)
  envG (InBattle _ _ env _) = env
  scenG (InBattle _ _ _ s)  = s
  wndG (InBattle _ w env scene) = w
  withSc s (InBattle c w env scene) = (InBattle c w env s)
  updateGame dt g@(InBattle c w env scene) = do
    v<- getView w
    (V2 x y) <- getPos $ scene^.protag
    (Vec2f cx cy) <- getViewCenter v
    let (c1,c2) = (0.9,0.1)
    setViewCenter v $ Vec2f (x*tSZ') (y*tSZ')
    setView w v
    nS <- updateScene dt (scenG g)
    let p = scene^.protag
    let checkPDth = do
                    lf <- getLife p
                    return $ toMaybe (lf <0) 1
    code <- checkPDth
    return $ withSc nS (InBattle code w env scene)

-- MENU --
  
cancelG :: (GameS MenuS) -> IO ()
cancelG g@(InMenu _ w env (MenuS m t _)) = do
  (m^.cancelAct) >?> execAction g

confirmG :: (GameS MenuS) -> IO (GameS MenuS)
confirmG g@(InMenu _ w env s@(MenuS m t _)) = do
  elems <- HIO.toList $ m^.mElems
  let activeElems = (filter (^.uiActive) $ map snd elems)
  let filtSel = \el@UIElem{uiSelect = f} ->  f s
  selecElems <- filterM filtSel activeElems
  nG <- case selecElems of
    [] ->(m^.defaultAct) >?>= g $ \a -> print "fm" >> mapAction g a
    _  -> return g
  let knot g el@UIElem{uiAction = a} = a >?>= g $  \a -> print "sm" >> mapAction g  a               
  print selecElems
  cG <- foldM knot nG selecElems 
  return cG

switchTo :: (GameEnv -> IO GMenu) -> GameS MenuS -> IO (GameS MenuS) 
switchTo mkMenu (InMenu c w gEnv menuS)= do
  print "changing menu"
  let oldMenu = menuS^.sMenu
  cleanMenu oldMenu gEnv
  newMenu <- mkMenu gEnv
  let nS = switchMenu newMenu menuS
  return (InMenu c w gEnv nS)

cursorInRange crd howMuch wL s  = do
  let c = s^.msCursor
  res <- case c of 
    Nothing  -> return False
    Just (V2 x y)  -> do
      let p = getUIPos crd
      return $ (distance (V2 (fromIntegral x-(wL)) (fromIntegral y)) p < howMuch)
  return res

updateCursor x y s =
  return $ s & msCursor .~ Just (V2 x y)

switchMenu newMenu s = s & sMenu.~ newMenu

-- BATTLE

handlePress :: KeyCode -> (GameS BattleS) -> IO (GameS BattleS)
handlePress code    gS@(InBattle _ wnd env s@(BattleS time cursor protag enemies tiles tilesData)) = do
  let skillNumber = elemIndex code [KeyX,KeyC,KeyV]
  skillNumber >?> \n -> do
    print n
    tryState (InSkill $ n +1) protag >> return ()
  let direction = find (==code) [KeyLeft,KeyRight]
  direction >?> \d -> case d of
    KeyLeft   -> tryState Walking protag >> setDir 4 protag >>return ()
    KeyRight  -> tryState Walking protag >> setDir 6 protag >>return ()
  return gS
handleRelease :: KeyCode -> (GameS BattleS) -> IO (GameS BattleS)
handleRelease code  gS@(InBattle _ wnd env s@(BattleS time cursor protag enemies tiles tilesData)) = do
  let direction = find (==code) [KeyLeft,KeyRight]
  direction >?> \d -> do
      (TS _ _ d')<-  getState protag
      when (d'==getDir d) $
        tryState Idle protag >> return ()
      return ()
   
  return gS




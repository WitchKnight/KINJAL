{-# LANGUAGE TemplateHaskell,DoAndIfThenElse, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}

module Init where

import GHC.Generics

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import Data.HashMap.Strict((!))
import qualified Data.HashMap.Strict as DHS
import qualified Data.HashTable.IO as HIO
import Data.Hashable
import Linear.V2
import Data.IORef

import Control.Lens
import Control.Monad

import Game
import GameData
import Lib
import Animations
import Conf
import UI
import View

import Actors
import App

thrust = Skill  {
  _finalPos = V2 0.5 0,
  _duration =0.8,
  _damage = 1,
  _skillData = [ (15,[]), 
                (5, [Displace (V2 10 0), HitBox (V2 10 0) c0]),
                (20,[Displace (V2 20 0), HitBox (V2 5 0) c0]),
                (10,[Displace (V2 10 0)])  
              ] 
}

spin = Skill  {
  _finalPos = c0,
  _duration =0.4,
  _damage = 2,
  _skillData = replicate 4 (4,[HitBox  (V2 (-5) (-1)) c0, Parry 4])
}

retaliation = Skill {
  _finalPos = c0,
  _duration =0.4,
  _damage = 1,
  _skillData = [ (7, [Invincible]), 
                (3, [Invincible,Displace (V2 5 0), HitBox c0 c0]), 
                (10,[Invincible,Displace (V2 10 0), HitBox c0 c0]),
                (5, [Displace (V2 5 0)])
              ]
}


defThrust = thrust  & finalPos .~ V2 (-0.5) 0
                    & skillData .~ [ (15,[]), 
                                      (5, [Displace (V2 (-10) 0), HitBox (V2 5 0) c0]),
                                      (20,[Displace (V2 (-20) 0), HitBox (V2 5 0) c0]),
                                      (10,[Displace (V2 (-10) 0)])  
                                    ] 

initChevalier gEnv = do
  sprtex <- err $ textureFromFile (imgPath ++ "thrust.png") Nothing
  hurtex <- err $ textureFromFile (imgPath ++ "hurtchev.png") Nothing
  dsprtex <- copy sprtex
  retex <- copy sprtex
  bntex <- err $ textureFromFile  (imgPath ++ "chevalier.png") Nothing
  wltex <- err $ textureFromFile  (imgPath ++ "walking.png") Nothing
  spntex <- err $ textureFromFile  (imgPath ++ "spin.png") Nothing
  spinanim <- getSkAnim spntex spin
  spranim <- getSkAnim sprtex thrust 
  dspranim <- getSkAnim dsprtex defThrust
  retanim <- getSkAnim retex retaliation
  idlanim <- initAnim' bntex (replicate 4 20)
  hurtanim <- initAnim' hurtex (replicate 4 20)
  wlanim <- initAnim' wltex (replicate 4 12)
  let anims = DHS.fromList [(Idle,idlanim),(Walking,wlanim),(Hurt,hurtanim)]
  let skills = [(thrust,spranim),(defThrust,dspranim),(spin,spinanim),(retaliation,retanim)]
  actor <- mkActor gEnv anims skills (V2 0 0) noAI
  return $ actor & aSpeed .~ 2
 
smash =  Skill  {
  _finalPos = V2 2 0,
  _duration =2,
  _damage = 5,
  _skillData = [ (30,[Displace c0]), 
                (25,[Displace (V2 15 0)]), 
                (10,[Displace (V2 30 0)]),
                (55,[Displace (V2 40 0),HitBox (V2 5 10) c0])
    ]

}

initOgrAnim gEnv = do
  bntex <- err $ textureFromFile (imgPath ++ "ogre.png") Nothing
  ogranim <- initAnim' bntex (replicate 4 20)
  return ogranim

initOgre gEnv =initOgre' gEnv 0
              

initOgre' gEnv i= do
  
  let ogreSight target ogre = do
        p1 <- getPos target
        p2 <- getPos ogre
        TS state dir time <- getState ogre
        when (state==Idle) $ do
          when (distance p1 p2 < 3) $
            case compare (p1^._x) (p2^._x) of
              GT ->tryState (InSkill 1)  ogre >> return ()
              LT ->tryState (InSkill 1)  ogre >> return () 
          when (distance p1 p2 < 5) $
            case compare (p1^._x) (p2^._x) of
              GT -> tryState (Walking)  ogre  >> setDir 6 ogre 
              LT -> tryState (Walking)  ogre  >> setDir 4 ogre 

        return ()
    
  let ogreAttack ogre= do
                  ts <- getState ogre
                  a <- case ts of 
                    TS Idle time d -> do
                      when (asSeconds time > 5) $ do
                        tryState (InSkill 1) ogre
                        return ()
                    _ -> return ()
                  return ()
  let ogreAI = noAI & always .~ ogreAttack
                    & onSight .~ ogreSight
  bntex     <- err $ textureFromFile (imgPath ++ "ogre.png") Nothing
  ogranim   <- initAnim' bntex (replicate 4 20)
  hurtex    <- err $ textureFromFile  (imgPath ++ "hurtogre.png") Nothing
  hurtanim  <- initAnim' hurtex (replicate 4 20)
  smatex<- err $ textureFromFile  (imgPath ++ "ogresmash.png") Nothing
  smanim <- getSkAnim smatex smash
  waltex<- err $ textureFromFile  (imgPath ++ "walkingogre.png") Nothing
  walkanim <- initAnim' waltex (replicate 4 18)
  let anims = DHS.fromList [(Idle,ogranim),(Hurt,hurtanim),(Walking,walkanim)]
  let skills = [(smash,smanim)]
  actor <- mkActor gEnv anims skills (V2 i 0) ogreAI
  modLife actor (+200)
  tryState (InSkill 1) actor
  setDir 4 actor

  return $ actor & aSpeed .~ 1.2


initDragon gEnv i = do
  drtex     <- err $ textureFromFile (imgPath ++ "animdragon.png") Nothing
  dranim    <- initSizedAnim drtex   (replicate 4 80) [] (V2 138 69) (V2 0 23)
  hurtex    <- err $ textureFromFile (imgPath ++ "hurtdragon.png") Nothing
  hurtanim  <- initSizedAnim hurtex [] [] (V2 138 69) (V2 0 23)
  let anims = DHS.fromList [(Idle,dranim),(Hurt,hurtanim)]
  let skills =  []
  actor <- mkSizedActor gEnv anims skills (V2 i 0) (V2 138 69) noAI
  return actor



initGame wnd = do
  gEnv <- startEnv
  menu <- mainMenu gEnv
  t <- newIORef timeZero
  return $ InMenu  Nothing wnd gEnv (MenuS menu t Nothing)





mkMenuEls :: [(UICoord,Int -> IO GUI)] -> [(UICoord,IO GUI)]
mkMenuEls stuff = mkMenuEls' 0 stuff        
mkMenuEls' _ [] = []             
mkMenuEls' n ((crd,mkEl):xs) = (crd, newEl):(mkMenuEls' (n+1) xs)
  where
    newEl = do
      el <- mkEl n
      wordSize <- (el^.uiLabel) >?>= (0 :: Float) $ \l -> do
        return $  fromIntegral $ (`div` 2) $ (length  $ l^.lText) * maybeOr (l^.cSize) 30
      return $ el {uiSelect = cursorInRange crd (wordSize/2) (wordSize/2) }


mainMenu :: GameEnv -> IO (GMenu)
mainMenu = basicMenu [ 
  ((V2 (V2 3 7) (V2 1 7)),  mkPlainText "KINJAL" (Just 50)),
  ((V2 (V2 1 2) (V2 1 3)),  mkActionText (OutsideAction 1) "Play" (Just 43)),
  ((V2 (V2 1 2) (V2 3 6)),  mkActionText (OutsideAction 0) "Quit" (Just 43)),
  ((V2 (V2 1 9) (V2 8 9)),  mkActionText (GameAction $ switchTo testMenu) "Kinjal Is Not Just a Loop" (Just 12))
  ]

testMenu :: GameEnv -> IO (GMenu)
testMenu gEnv = do
  anim1 <- initOgrAnim gEnv
  basicMenu [
              ((V2 (V2 1 6) (V2 1 6)),  \n -> mkPlainPic anim1 ),
              ((V2 (V2 1 3) (V2 4 5)),  mkActionText (GameAction $ switchTo mainMenu) "click me" (Just 23)),
              ((V2 (V2 6 7) (V2 1 2)),  mkPlainText "ok" (Just 40))
              ] gEnv
 

overMenu :: GameEnv -> IO (GMenu)
overMenu = basicMenu [ 
  ((V2 (V2 3 7) (V2 1 7)),  mkPlainText "GAME OVER" (Just 60)),
  ((V2 (V2 1 2) (V2 1 3)),  mkActionText (OutsideAction 1) "Try Again" (Just 43)),
  ((V2 (V2 1 2) (V2 3 6)),  mkActionText (OutsideAction 3) "Quit" (Just 43)),
  ((V2 (V2 1 9) (V2 8 9)),  mkActionText (GameAction $ switchTo testMenu) "Kinjal Is Not Just a Loop" (Just 12))
  ]


outAction 0 = \game -> beginBattle game

basicMenu cels gEnv = do
  cels' <- mapM (\(x,y') -> do
                              y <- y'
                              return (x,y)) $ mkMenuEls cels
  initMenu Nothing Nothing cels' gEnv


gameOver  :: GameS BattleS -> IO  (GameS MenuS) 
gameOver  (InBattle _ w gEnv bS) = do
  v <- getView w
  resetView v $ FloatRect 0 0 gWIDTH' gHEIGHT'
  setView w v
  menu <- overMenu gEnv
  t <- newIORef timeZero
  return $ InMenu  Nothing w gEnv (MenuS menu t Nothing)

beginBattle :: GameS MenuS -> IO  (GameS BattleS) 
beginBattle (InMenu _ w gEnv menuS) = do
  v <- getView w
  zoomView v 0.5
  setView w v
  t <- newIORef timeZero
  let cursor = Nothing
  prot <- initChevalier gEnv
  let enems i = case (i==0) of 
                False -> do
                  ogre <- initOgre' gEnv i
                  rest <- enems (i-1)
                  return $  ogre:rest
                True -> return []
  enems' <-enems 10
  dragon<- initDragon gEnv 14
  setDir 4 dragon
  enemies <- HIO.fromList $ zip [ 0..] $ dragon:enems'

  grTex   <- err $ textureFromFile (imgPath ++ "ground.png") Nothing
  grAnim <- initAnim' grTex []
  grI <- addAnim' gEnv grAnim
  tiles <- HIO.fromList $ [(V2 x 1, Ground) | x <- [-20..20]]
  tileData <- HIO.fromList $ [(Ground,grI)]
  let bS = BattleS t cursor prot enemies tiles tileData
  return $ (InBattle Nothing w gEnv bS)

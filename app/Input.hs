{-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Input where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock

import Control.Lens.Operators
import Debug.Trace
import Control.Concurrent.Async hiding (cancel)
import Control.Monad
import Linear.V2

import Lib
import Conf
import Actors
import Init
import App



-- handlePress :: KeyCode -> Bool -> Bool -> Bool -> Bool -> World -> IO ()
-- handlePress code ctrl alt shift sys world
--   | attackKey   = attackP world thrust
--   | cancelKey   = setState bonh Idle
--   | dirKey      = setMove 
--   | otherwise   = return ()
--   where
--     bonh = world^.protag
--     attackKey   = code `elem` [KeyX,KeyReturn]
--     cancelKey   = code `elem` [KeyEscape]
--     dirKey      = code `elem` [KeyLeft,KeyRight]
--     setMove = case code of 
--       KeyLeft   -> beginMove 4 bonh
--       KeyRight  -> beginMove 6 bonh
--       _         -> return ()


-- handleRelease :: KeyCode -> Bool -> Bool -> Bool -> Bool -> World -> IO ()
-- handleRelease code ctrl alt shift sys world
--   | dirKey      = unsetMove
--   | otherwise   = return ()
--   where
--     bonh = world^.protag
--     dirKey      = code `elem` [KeyLeft,KeyRight]
--     unsetMove = do
--       dir <- getDir bonh
--       if code ==  KeyLeft && dir == 4 then
--         beginMove 5 bonh
--       else if code ==  KeyRight && dir == 6 then
--         beginMove 5 bonh
--       else
--         return ()
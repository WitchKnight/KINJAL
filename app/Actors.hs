{-# LANGUAGE ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Actors where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.List (find)
import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game

type Direction = Int

data ActorState =   Idle
                  | InSkill Int
                  | Walking
                  | Hurt 
                  deriving (Show,Read,Eq,Ord,Generic)


instance Hashable ActorState

data TotalState = TS ActorState Time Direction deriving (Show,Read,Eq,Ord,Generic)
  
data SkillAttrib = Invincible
                  | Parry Int
                  | HitBox Coord Coord
                  | Displace Coord
                  deriving (Show,Read,Eq,Ord,Generic)

instance Hashable SkillAttrib
invincible :: [SkillAttrib] -> Bool
invincible = elem Invincible


displace :: [SkillAttrib] -> Maybe SkillAttrib
displace xs = find f xs
  where
    f x = case x of
      Displace coord -> True
      _ -> False

hitBoxes :: [SkillAttrib] -> [SkillAttrib]
hitBoxes xs = [ HitBox x y | HitBox x y <- xs ]

parry :: [SkillAttrib] -> Maybe SkillAttrib
parry xs =  find f xs
  where
    f x = case x of
      Parry x -> True
      _ -> False




data Skill = Skill {
  _duration :: Float, -- in seconds
  _skillData :: [(Int, [SkillAttrib])],-- number of frames, how much the player moves, where the point is if any . 
  _finalPos :: Coord,
  _damage   :: Int
} deriving (Show,Read,Eq,Ord,Generic)


makeLenses ''Skill

instance Hashable Skill

data ActorAI = ActorAI {
  _onHit        :: Actor -> Actor -> IO (),
  _onSight      :: Actor -> Actor -> IO (),
  _always       :: Actor -> IO ()
}

noAI = ActorAI (\_ _  -> return ()) (\_ _ ->return ()) (\_ -> return ())

data Actor = Actor {
  _actSize  :: Coord,
  _aPos     :: IORef Coord,
  _aSkill   :: HIO.LinearHashTable Int Skill,
  _aState   :: IORef TotalState,
  _aAnim    :: HIO.LinearHashTable ActorState Int,
  _aLife    :: IORef Int,
  _aSpeed   :: Float,
  _aI       :: ActorAI 
}

makeLenses ''Actor
makeLenses ''ActorAI


getPos bonh = do
  pos <- readIORef $ bonh^.aPos
  return pos

getRealPos act = do
  pos <- getPos act
  (TS st t d) <- getState act
  offset <- case st of 
    InSkill i -> do
      skl <- HIO.lookup (act^.aSkill) i
      skl >?>= c0 $  \s -> do
        let sData = getSklSkillData s t
        let pmPos = displace sData
        pmPos >?>= c0 $ \(Displace c) -> return c
    _ -> return c0
  return $ (fmap (*tSZ') pos) + (negOrPos (d==4)  offset)

getLife actor = readIORef $ actor^.aLife

getState bonh = do
  state <- readIORef (bonh^.aState) 
  return state

modState bonh = modifyIORef (bonh^.aState)

modLife bonh = modifyIORef (bonh^.aLife)

setDir d actor= do
  TS st _ old<- readIORef $ actor^.aState 
  modState actor (\(TS s t _) -> TS s t d)

resetDir actor= do
  modState actor (\(TS s t _) -> TS s t 5)

knockback actor pow d= do
  when (d==4)  $ do
    modifyIORef (actor^.aPos) (+ (V2 ( - pow) 0 ))
  when (d==6) $ do
    modifyIORef (actor^.aPos) (+ (V2 pow 0 ))

moveActorX actor dx = do
  modifyIORef (actor^.aPos) (+ (V2 dx 0 ))

softState actor state = do
  p <- getRealPos actor
  writeIORef (actor^.aPos) $ toAPos p
  setState actor state

setState actor state = do
  TS st _ old<- readIORef $ actor^.aState 
  writeIORef (actor^.aState) (TS state 0 old)

updateActor dt actor = do
  (actor^.aI.always) actor
  let updateTS  = (\(TS st time d) -> TS st (time+dt) d)
  state <- getState actor
  a <- case state of
    TS (InSkill i) time d ->do
      skill <- HIO.lookup (actor^.aSkill) i
      skill >?>/ (setState actor (Idle)) $  \skill -> do
        when (asSeconds time > skill^.duration) $ do
          setState actor (Idle)
          when (d==4)  $ do
            modifyIORef (actor^.aPos) (+ (0 - skill^.finalPos))
          when (d==6) $ do
            modifyIORef (actor^.aPos) (+ (skill^.finalPos))
    TS Walking time d -> do 
      when (d==4)  $ do
        modifyIORef (actor^.aPos) (+ (V2 ( - (actor^.aSpeed*asSeconds dt)) 0 ))
      when (d==6) $ do
        modifyIORef (actor^.aPos) (+ (V2 (actor^.aSpeed*asSeconds dt) 0 ))
      when (asSeconds time > 2) $ do
        setState actor Idle
    TS Hurt time d -> do
      when (asSeconds time > 0.2) $ do
        setState actor Idle
    _   -> return ()
  modState actor updateTS


mkSizedActor :: GameEnv -> DHS.HashMap ActorState Animation -> [(Skill,Animation)] -> Coord -> Coord -> ActorAI ->IO Actor
mkSizedActor gEnv anims skills coord size ai= do
  actor <- mkActor gEnv anims skills coord  ai
  return $  actor & actSize .~ size


mkActor :: GameEnv -> DHS.HashMap ActorState Animation -> [(Skill,Animation)] -> Coord -> ActorAI -> IO Actor
mkActor gEnv anims actorSkills coord ai= do
  let anims' = DHS.toList anims
  refAnims <- HIO.new 
  let totSkills = zip [1..] actorSkills
  forM anims' $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (refAnims) state iD
  forM totSkills $ \(i,(skill,anim)) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (refAnims) (InSkill i) iD
  refSkills <- HIO.fromList $  [(x,y) | (x,(y,_)) <- totSkills]
  apos <- newIORef coord
  stat <- newIORef  $ TS  Idle 0 6
  life <- newIORef 100
  return $ Actor (V2 tSZ' tSZ') apos refSkills stat refAnims life 2 ai

displayAct tg gEnv actor = do
  TS st time d<- readIORef $ actor^.aState
  i <- HIO.lookup (actor^.aAnim) st
  V2 x y <- readIORef $ actor^.aPos
  let transf = translation (x*tSZ') (y*tSZ')
  i >?> \i -> do
    (anim :: Maybe Animation) <- getAnim gEnv i
    anim >?>/ (print "no anim") $ \anim -> do
      withAnimDir d anim >>=dwtat tg time transf


toAPos pos = fmap (/tSZ') pos


getSkAnim tex skill = 
  let hp = (skill^.skillData)
      durations = map (\x -> x^._1) hp
      attribs = map (\x -> x^._2) hp
      mov = map f attribs
      f [] =  c0
      f (Displace crd:xs) = crd
      f (x:xs) = f xs in
  initAnim tex durations mov

useSkill skillId actor = tryState (InSkill skillId)

tryState state actor = do
  TS currentState time _ <- getState actor
  a <- case currentState of
    InSkill _   -> return False
    _ -> do
      when (currentState/=state)  $ do
        setState actor state
      return True
  return a

getSklSkillData s t= hPnt
  where
    hp = s ^.skillData
    durations = map (\x -> x^._1) hp
    index = getIndex durations t 
    (dur,hPnt) = hp !! index



getDir KeyLeft = 4
getDir KeyRight = 6
getDir _ = 5
module Lib where

import Linear.V2
import qualified Data.HashTable.IO as HIO

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import Control.Monad
import Control.Lens hiding (transform)
import Conf

someFunc :: IO ()
someFunc = putStrLn "someFunc"

type Coord = V2 Float

flipx (V2 x y) = V2 (not x) y
flipy (V2 x y) = V2 x (not y)

centerSprite spr = do
    tr <- getTextureRect spr
    let w = iwidth tr
    let h = iheight tr
    let vec = Vec2f ((fromIntegral w)/2) ((fromIntegral h)/2) 
    setOrigin spr vec

c0 :: Coord
c0 = V2 0 0

c1 :: Coord
c1 = V2 1 1

distance (V2 x y) (V2 x1 y1) = sqrt $ (x-x1)**2 + (y-y1)**2

maybeToIO :: (a -> IO ()) -> Maybe a -> IO () 
maybeToIO f x= case x of
  Just y -> do
    f y
  Nothing -> return ()

maybeToIO' :: b -> (a -> IO b) -> Maybe a -> IO b 
maybeToIO' c f x= case x of
  Just y -> do
    f y
  Nothing -> return c

maybeToIOor :: (a -> IO ()) -> IO () -> Maybe a -> IO () 
maybeToIOor f or x= case x of
  Just y -> do
    f y
  Nothing -> or
  
(>?>) :: Maybe a -> (a -> IO ()) -> IO ()
(>?>) x f = return x >>= maybeToIO f

tryCompute x or f  = return x >>= maybeToIO' or f
(>?>=) = tryCompute

(>?>/) x or f = return x >>= maybeToIOor f or

getMaxId :: HIO.LinearHashTable Int a -> IO Int
getMaxId hashtable = do
  ll <- HIO.toList hashtable
  let ks = [k | (k,v) <- ll]
  return $ case ks of [] -> 0
                      _   -> maximum ks

discard f = \a -> f  a >> return () 

getUIPos ( V2 (V2 x1 x2) (V2 y1 y2) ) = 
  V2 (gWIDTH'*(x1/x2)) $ gHEIGHT'*y1/y2

maybeOr (Just x) y = x
maybeOr Nothing y = y

negOrPos True x = -x
negOrPos False x = x

invertX :: Coord -> Coord
invertX (V2 x y) = V2 (-x) y

touches :: Coord -> Coord -> Coord -> Bool
touches attackPos actorPos actorSize = 
  attackPos^._x > actorPos^._x - (actorSize^._x)/2 &&
  attackPos^._y > actorPos^._y - (actorSize^._y)/2 &&
  attackPos^._x < actorPos^._x + (actorSize^._x)/2 &&
  attackPos^._y < actorPos^._y + (actorSize^._y)/2 

toMaybe True x = Just x
toMaybe False x = Nothing

whenl action cond = when cond action